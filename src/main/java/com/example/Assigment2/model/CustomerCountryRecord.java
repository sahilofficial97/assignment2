package com.example.Assigment2.model;

import com.example.Assigment2.model.Customer;
import lombok.NoArgsConstructor;

public record CustomerCountryRecord(String country, int mostCustomer) {}
