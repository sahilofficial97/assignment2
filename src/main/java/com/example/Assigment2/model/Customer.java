package com.example.Assigment2.model;

public record Customer(int id, String firstName, String lastName, String country, String postalCode, String phone, String email) {}
