package com.example.Assigment2.model;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CustomerCountry {

    private String country;
    private int mostCustomer;
}
