package com.example.Assigment2.model;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class PopularGenre {
    private String genre;
    private int amountOfTrackBoughtWithGenre;
}
