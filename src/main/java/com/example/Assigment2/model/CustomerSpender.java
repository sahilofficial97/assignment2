package com.example.Assigment2.model;

import lombok.Data;

@Data
public class CustomerSpender {
    private int id;
    private String firstName;
    private int amountSpend;
}
