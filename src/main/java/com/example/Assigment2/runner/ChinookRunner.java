package com.example.Assigment2.runner;

import com.example.Assigment2.model.Customer;
import com.example.Assigment2.repository.*;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Repository;

@Repository
public class ChinookRunner implements CommandLineRunner {

    final CustomerRepository chinookDAO;
    final CustomerCountryRepository chinookDA1;
    final CountryCustomerRepository countryCustomerRepository;

    final CustomerSpenderRepository customerSpenderRepository;

    final PopularGenreRepository popularGenreRepository;

    public ChinookRunner(CustomerRepository chinookDAO, CustomerCountryRepository chinookDA1, CountryCustomerRepository customerRepository, CustomerSpenderRepository customerSpenderRepository, PopularGenreRepository popularGenreRepository){
        this.chinookDAO = chinookDAO;
        this.chinookDA1 = chinookDA1;
        this.countryCustomerRepository=customerRepository;
        this.customerSpenderRepository=customerSpenderRepository;
        this.popularGenreRepository=popularGenreRepository;
    }


    @Override
    public void run(String... args) throws Exception {
        //2.1
        chinookDAO.getAll();
        //2.2
        Customer dbUser=chinookDAO.getByID(2);
        //2.3
        Customer dbUser2=chinookDAO.getByName("Daan","Peeters");
        //2,4
        chinookDAO.getFewCustomer();
        //2.5
        Customer customer = new Customer(60,"Roel","Knakkers","Swartbroek","6008PL", "+31 64813797131","test@hotmail.com");
        chinookDAO.create(customer);
        //2.6
        chinookDAO.update(customer);
        //2.7
        countryCustomerRepository.getCountryWithMostCustomer();
        //2,8
        customerSpenderRepository.getMostSpendCustomer();
        //2.9
        popularGenreRepository.getMostBoughtGenre();

    }
}
