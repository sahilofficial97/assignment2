package com.example.Assigment2;

import com.example.Assigment2.model.Customer;
import com.example.Assigment2.repository.*;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Assigment2Application {

	public static void main(String[] args) {
		SpringApplication.run(Assigment2Application.class, args);
	}

}
