package com.example.Assigment2.repository;

import com.example.Assigment2.model.PopularGenre;

public interface PopularGenreRepository extends CrudRepository<Integer, PopularGenre>{
}
