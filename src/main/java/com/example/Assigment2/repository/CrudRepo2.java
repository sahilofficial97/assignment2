package com.example.Assigment2.repository;

import java.util.List;

public interface CrudRepo2 <ID, T>{
    T getByID(ID id);
    List<T> getAll();

    List<T> getFewCustomer();

    T CountryWithMostCustomer();


    T getByName(String firstName,String lastName);

    int create(T object);
    int update(T object);
    void delete(T object);

}
