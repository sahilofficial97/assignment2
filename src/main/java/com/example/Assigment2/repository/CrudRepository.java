package com.example.Assigment2.repository;

public interface CrudRepository <ID, T>{
    T getMostBoughtGenre();
    T getMostSpendCustomer();
    T getCountryWithMostCustomer();
}
