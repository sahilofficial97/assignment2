package com.example.Assigment2.repository;

import com.example.Assigment2.model.CustomerCountryRecord;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.List;

@Repository

public class CustomerCountryRepositoryImpl implements CustomerCountryRepository{
    private final String url;
    private final String username;
    private final String password;

    public CustomerCountryRepositoryImpl(
            // injection from the application.properties
            // Injection in constructor is always better is the last check of Spring
            @Value("${spring.datasource.url}") String url,
            @Value("${spring.datasource.username}") String username,
            @Value("${spring.datasource.password}") String password) {
        this.url = url;
        this.username = username;
        this.password = password;
//        testConnection();
    }

    @Override
    public CustomerCountryRecord CountryWithMostCustomer() {
        CustomerCountryRecord customerCountry = null;
        String sql = "SELECT country, COUNT(*) FROM customer GROUP BY country Order By COUNT(*) DESC limit 1";
        try(Connection connection = DriverManager.getConnection(url, username, password)){
            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet result = statement.executeQuery();
            while (result.next()){
                customerCountry = new CustomerCountryRecord(
                  result.getString("country"),
                  result.getInt("count")
                );
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return customerCountry;
    }






    @Override
    public CustomerCountryRecord getByID(Integer integer) {
        return null;
    }

    @Override
    public List<CustomerCountryRecord> getAll() {
        return null;
    }

    @Override
    public List<CustomerCountryRecord> getFewCustomer() {
        return null;
    }


    @Override
    public CustomerCountryRecord getByName(String firstName, String lastName) {
        return null;
    }

    @Override
    public int create(CustomerCountryRecord object) {
        return 0;
    }

    @Override
    public int update(CustomerCountryRecord object) {
        return 0;
    }

    @Override
    public void delete(CustomerCountryRecord object) {

    }

}
