package com.example.Assigment2.repository;

import com.example.Assigment2.model.CustomerCountry;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.sql.*;

@Slf4j
@Repository
public class CountryCustomerRepositoryImp implements CountryCustomerRepository{

    private final String url;
    private final String username;
    private final String password;

    public CountryCustomerRepositoryImp(
            // injection from the application.properties
            // Injection in constructor is always better is the last check of Spring
            @Value("${spring.datasource.url}") String url,
            @Value("${spring.datasource.username}") String username,
            @Value("${spring.datasource.password}") String password) {
        this.url = url;
        this.username = username;
        this.password = password;
//        testConnection();
    }
    //This method will get a country with the most customer
    @Override
    public CustomerCountry getCountryWithMostCustomer() {
        CustomerCountry customerCountry = new CustomerCountry();
        String sql = "SELECT country, COUNT(*) FROM customer GROUP BY country Order By COUNT(*) DESC limit 1";
        try(Connection conn = DriverManager.getConnection(url, username,password)) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);
            // Execute statement
            ResultSet result = statement.executeQuery();
            // Handle result
            while(result.next()) {
                log.info(result.getString("country")+" -> "+result.getString("count"));
                customerCountry.setMostCustomer(result.getInt("count"));
                customerCountry.setCountry(result.getString("country"));

            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customerCountry;
    }

    @Override
    public CustomerCountry getMostBoughtGenre() {
        return null;
    }

    @Override
    public CustomerCountry getMostSpendCustomer() {
        return null;
    }
}
