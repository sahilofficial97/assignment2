package com.example.Assigment2.repository;

import com.example.Assigment2.model.CustomerSpender;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.sql.*;

@Slf4j
@Repository
public class CustomerSpenderRepositoryImp implements CustomerSpenderRepository{

    private final String url;
    private final String username;
    private final String password;

    public CustomerSpenderRepositoryImp(
            // injection from the application.properties
            // Injection in constructor is always better is the last check of Spring
            @Value("${spring.datasource.url}") String url,
            @Value("${spring.datasource.username}") String username,
            @Value("${spring.datasource.password}") String password) {
        this.url = url;
        this.username = username;
        this.password = password;
//        testConnection();
    }

    //this method will get the customer that spends the most money
    public CustomerSpender getMostSpendCustomer() {
        CustomerSpender customerSpender=new CustomerSpender();
        String sql = "SELECT i.customer_id,c.first_name, sum(i.total) FROM invoice i inner join customer c on i.customer_id = c.customer_id Group By i.customer_id,c.first_name Order By sum(i.total) DESC limit 1";
        try(Connection conn = DriverManager.getConnection(url, username,password)) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);
            // Execute statement
            ResultSet result = statement.executeQuery();
            // Handle result
            while(result.next()) {
                log.info(result.getString("first_name")+" -> amount spend:"+result.getInt("sum"));
                customerSpender.setFirstName(result.getString("first_name"));
                customerSpender.setAmountSpend(result.getInt("sum"));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customerSpender;
    }

    @Override
    public CustomerSpender getMostBoughtGenre() {
        return null;
    }

    @Override
    public CustomerSpender getCountryWithMostCustomer() {
        return null;
    }
}
