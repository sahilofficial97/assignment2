package com.example.Assigment2.repository;

import com.example.Assigment2.model.Customer;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class CustomerRepositoryImpl implements CustomerRepository {

    private final String url;
    private final String username;
    private final String password;

    public CustomerRepositoryImpl(
            // injection from the application.properties
            // Injection in constructor is always better is the last check of Spring
            @Value("${spring.datasource.url}") String url,
            @Value("${spring.datasource.username}") String username,
            @Value("${spring.datasource.password}") String password) {
        this.url = url;
        this.username = username;
        this.password = password;
//        testConnection();
    }


    @Override
    public Customer getByID(Integer id) {
        String sql = "SELECT customer_id, first_name, last_name, country, postal_code, phone, email FROM customer WHERE customer_id = ?";
        Customer customer = null;
        try(Connection connection = DriverManager.getConnection(url, username, password)){
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, id);
            ResultSet result = statement.executeQuery();
            while (result.next()){
                customer = new Customer(
                        result.getInt("customer_id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getString("country"),
                        result.getString("postal_code"),
                        result.getString("phone"),
                        result.getString("email")
                );
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return customer;
    }

    @Override
    public List<Customer> getAll() {
        List<Customer>  customerList = new ArrayList<>();
        Customer customer = null;
        String sql = "SELECT customer_id, first_name, last_name, country, postal_code, phone, email FROM customer";
        try (Connection connection = DriverManager.getConnection(url, username, password)){
            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet result = statement.executeQuery();
            while (result.next()){
                 customer = new Customer(
                         result.getInt("customer_id"),
                         result.getString("first_name"),
                         result.getString("last_name"),
                         result.getString("country"),
                         result.getString("postal_code"),
                         result.getString("phone"),
                         result.getString("email")
                );
                customerList.add(customer);
            }
            for (Customer customerDetail : customerList){
                System.out.print("Id: "+customerDetail.id()+", ");
                System.out.print("Firstname: "+customerDetail.firstName()+", ");
                System.out.print("Lastname: "+customerDetail.lastName()+", ");
                System.out.print("Country: "+customerDetail.country()+", ");
                System.out.print("Poscal code: "+customerDetail.postalCode()+", ");
                System.out.print("Phone: "+customerDetail.phone()+ ", ");
                System.out.print("Email: "+customerDetail.email()+ ", ");
                System.out.println();
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return customerList;
    }

    @Override
    public List<Customer> getFewCustomer() {
        List<Customer>  customerList = new ArrayList<>();
        Customer customer = null;
        String sql = "SELECT customer_id, first_name, last_name, country, postal_code, phone, email FROM customer limit 7 offset 2";
        try (Connection connection = DriverManager.getConnection(url, username, password)){
            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet result = statement.executeQuery();
            while (result.next()){
                customer = new Customer(
                        result.getInt("customer_id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getString("country"),
                        result.getString("postal_code"),
                        result.getString("phone"),
                        result.getString("email")
                );
                customerList.add(customer);
            }
            for (Customer customerDetail : customerList){
                System.out.print("Id: "+customerDetail.id()+", ");
                System.out.print("Firstname: "+customerDetail.firstName()+", ");
                System.out.print("Lastname: "+customerDetail.lastName()+", ");
                System.out.print("Country: "+customerDetail.country()+", ");
                System.out.print("Poscal code: "+customerDetail.postalCode()+", ");
                System.out.print("Phone: "+customerDetail.phone()+ ", ");
                System.out.print("Email: "+customerDetail.email()+ ", ");
                System.out.println();
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return customerList;
    }

    @Override
    public Customer CountryWithMostCustomer() {
        return null;
    }

    @Override
    public Customer getByName(String firstName, String lastName) {
        String sql = "SELECT customer_id, first_name, last_name, country, postal_code, phone, email FROM customer WHERE first_name LIKE ? OR last_name LIKE ?";
        Customer customer = null;
        try(Connection connection = DriverManager.getConnection(url, username, password)){
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, firstName);
            statement.setString(2, lastName);
            ResultSet result = statement.executeQuery();
            while (result.next()){
                customer = new Customer(
                        result.getInt("customer_id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getString("country"),
                        result.getString("postal_code"),
                        result.getString("phone"),
                        result.getString("email")
                );
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return customer;

    }

    @Override
    public int create(Customer customer) {
        String sql = "INSERT INTO customer (first_name, last_name, country, postal_code, phone, email ) VALUES (?,?,?,?,?,?)";
        int result;
        try (Connection connection = DriverManager.getConnection(url, username, password)){
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, customer.firstName());
            statement.setString(2, customer.lastName());
            statement.setString(3, customer.country());
            statement.setString(4, customer.postalCode());
            statement.setString(5, customer.phone());
            statement.setString(6, customer.email());
            result = statement.executeUpdate();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return result;
    }

    @Override
    public int update(Customer customer) {
        String sql = "UPDATE customer SET first_name = ?, last_name = ?, country = ?, postal_code = ?, phone = ?, email = ? WHERE customer_id = ? ";
        int result;
        try (Connection connection = DriverManager.getConnection(url, username, password)){
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, customer.firstName());
            statement.setString(2, customer.lastName());
            statement.setString(3, customer.country());
            statement.setString(4, customer.postalCode());
            statement.setString(5, customer.phone());
            statement.setString(6, customer.email());
            statement.setInt(7, customer.id());
            result = statement.executeUpdate();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return result;
    }

    @Override
    public void delete(Customer object) {

    }
}
