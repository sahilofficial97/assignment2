package com.example.Assigment2.repository;

import com.example.Assigment2.model.CustomerCountry;
import com.example.Assigment2.model.CustomerCountryRecord;


public interface CustomerCountryRepository extends CrudRepo2<Integer, CustomerCountryRecord>{}
