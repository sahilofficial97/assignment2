package com.example.Assigment2.repository;

import com.example.Assigment2.model.PopularGenre;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.sql.*;

@Slf4j
@Repository
public class PopularGenreRepositoryImp implements PopularGenreRepository {

    private final String url;
    private final String username;
    private final String password;

    public PopularGenreRepositoryImp(
            // injection from the application.properties
            // Injection in constructor is always better is the last check of Spring
            @Value("${spring.datasource.url}") String url,
            @Value("${spring.datasource.username}") String username,
            @Value("${spring.datasource.password}") String password) {
        this.url = url;
        this.username = username;
        this.password = password;
//        testConnection();
    }
    //This method will get the most popular genre bouht by user
    @Override
    public PopularGenre getMostBoughtGenre() {
        PopularGenre popularGenre =new PopularGenre();
        String sql = "SELECT g.name, sum(t.genre_id) FROM invoice_line i inner join track t on i.track_id = t.track_id inner join genre g on t.genre_id = g.genre_id GROUP BY g.genre_id Order By sum(t.genre_id) desc limit 1";
        try(Connection conn = DriverManager.getConnection(url, username,password)) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);
            // Execute statement
            ResultSet result = statement.executeQuery();
            // Handle result
            while(result.next()) {
                log.info(result.getString("name")+" -> Amount bought "+result.getString("sum"));
                popularGenre.setGenre(result.getString("name"));
                popularGenre.setAmountOfTrackBoughtWithGenre(result.getInt("sum"));

            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return popularGenre;
    }

    @Override
    public PopularGenre getMostSpendCustomer() {
        return null;
    }

    @Override
    public PopularGenre getCountryWithMostCustomer() {
        return null;
    }
}
