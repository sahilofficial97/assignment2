package com.example.Assigment2.repository;

import com.example.Assigment2.model.CustomerCountry;

public interface CountryCustomerRepository extends CrudRepository<Integer, CustomerCountry> {

}
