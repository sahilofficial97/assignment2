# Assignment 2 : Data Persistence and Access


## Getting started

This project is an example how to work with a Database and get data from it. We made couple of SQL querys to get the right data from the database. 

In the extra folders you can see the stand alone SQL querys from Apendix A.


## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/sahilofficial97/assignment2.git
git branch -M main
git push -uf origin main
```
## Team:

- Sahil Mankani
- Marco Beckers
