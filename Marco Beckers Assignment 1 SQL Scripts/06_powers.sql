/*Insert data in the power table and in the linking superheropower table*/

INSERT INTO power (name,description) VALUES ('Shoot', 'You can shoot anything from web to gun');
INSERT INTO power (name,description) VALUES ('Smash', 'Smash your enemy to the ground');
INSERT INTO power (name,description) VALUES ('Fly', 'Fly arround the city and protect civilians');
INSERT INTO power (name,description) VALUES ('Strongweb', 'Webstrings they can hold everything');

INSERT INTO superheropower(hero_id,power_id) VALUES (1,4);
INSERT INTO superheropower(hero_id,power_id) VALUES (2,2);
INSERT INTO superheropower(hero_id,power_id) VALUES (3,3);
INSERT INTO superheropower(hero_id,power_id) VALUES (1,3);
INSERT INTO superheropower(hero_id,power_id) VALUES (3,1);

