/*Table superheropower*/
/* Ensure we don't have re-create table SuperHero*/

DROP TABLE IF EXISTS superheropower;

CREATE TABLE superheropower(
    hero_id int REFERENCES superhero,
    power_id int REFERENCES power,
    PRIMARY KEY (hero_id,power_id)
);