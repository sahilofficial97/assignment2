/*Table superhero*/
/* Ensure we don't have re-create table SuperHero*/
DROP TABLE IF EXISTS superhero;

CREATE TABLE superhero(
    id serial PRIMARY KEY,
    name varchar(100) not NULL,
    alias varchar(100),
    origin varchar(100)
	);

/*Table assistant*/
/* Ensure we don't have re-create table assistant*/
DROP TABLE IF EXISTS assistant;

CREATE TABLE assistant(
    id serial PRIMARY KEY,
    name varchar(100) not NULL
);

/*Table power*/
/* Ensure we don't have re-create table power*/
DROP TABLE IF EXISTS power;

CREATE TABLE power(
    id serial PRIMARY KEY,
    name varchar(100),
    description varchar(100) 
);

