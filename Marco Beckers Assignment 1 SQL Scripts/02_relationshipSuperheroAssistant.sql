/* Make a foreign key of the superhero in the assistant table */
ALTER TABLE assistant
ADD hero_id int REFERENCES superhero;

