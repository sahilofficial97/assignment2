ALTER TABLE assistant
ADD superHeroId INTEGER;

ALTER TABLE assistant
ADD FOREIGN KEY (superHeroId) REFERENCES superhero(id);