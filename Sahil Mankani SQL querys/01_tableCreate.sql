create table superhero(
	id integer primary key,
	name varchar(255),
	alias varchar(255),
	origin varchar(255)
);

create table assistant(
	id integer primary key,
	name varchar(255)
);

create table power(
	id integer primary key,
	name varchar(255),
	description varchar(255)
);
