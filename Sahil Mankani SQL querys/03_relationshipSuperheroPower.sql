create table superheroPower(
	superheroid integer,
	powerid integer
);

ALTER TABLE superheroPower
ADD FOREIGN KEY (superheroid) REFERENCES superhero(id),
ADD FOREIGN KEY (powerid) REFERENCES power(id);
